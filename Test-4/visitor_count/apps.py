from django.apps import AppConfig


class VisitorCountConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'visitor_count'
