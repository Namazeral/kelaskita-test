from django.shortcuts import render as rnd
import json
from config.settings import BASE_DIR

# Create your views here.
def visitor_count(request):
    # Get the json file
    file_path = str(BASE_DIR) + '/config_file/count_data'
    print(file_path)
    try:
        # open json file
        json_data = open(file_path)
        data = json.load(json_data)
        # update the json file
        visitor_count = data.get('visitor_count')
        n = 1 if not visitor_count else int(data['visitor_count']) + 1
        data['visitor_count'] = n
        with open(file_path, 'w') as fp:
            json.dump(data, fp)
    except:
        # create json file if the file not found
        with open(file_path, 'w') as outfile:
            json.dump({"visitor_count": 1}, outfile)
        n = 1

    # change the format to ordinal
    ordinal_n = str(n) + ("th" if 4 <= n % 100 <= 20 else {1: "st", 2: "nd", 3: "rd"}.get(n % 10, "th"))
    return rnd(request, 'visitor_count.html', {
        'visitor_count': ordinal_n,
    })