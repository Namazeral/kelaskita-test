# TEST KELASKITA
- create your virtualenv and activate your virtual env
> ``` virtualenv .venv ```<br/>
> ``` source .venv/bin/activate ```
- update packages
> ``` pip install -r requirements.txt ```

## TEST 1
Membalikkan setiap kata pada suatu kekalimat<br/>
Ex:
> ``` iadab itsap ulalreb -> bada pasti berlalu ```

###Step:
> ``` python3 Test-1/reverse_string.py ``` <br/>
> ``` Enter the string: iadab itsap ulalreb ```<br/>
> ``` badai pasti berlalu ```

## TEST 2
Memunculkan angka dari 1 hingga 100 dengan kondisi <br/>
    - sebuah angka habis dibagi 3, maka tampilkan kata Fizz di sebelahnya. <br/>
    - angkanya habis dibagi 5, tampilkan kata Buzz di sebelahnya. <br/>
    - angka tersebut habis dibagi 3 dan habis dibagi 5, tampilkan kata FizzBuzz di sebelah angka tersebut. <br/>

###Step:
> ``` python3 Test-2/Fizzbuzz.py ``` <br/>

## TEST 3
Program yang menghasilkan deret angka sederhana yang susunan angkanya merupakan penjumlahan dari dua angka sebelumnya
Ex:
> ``` [0,1,1,2,3,5,8,13,21] ```

###Step:
> ``` python3 Test-3/fibonacci_sequence.py ``` <br/>
> ``` Input the limit(default is 9):9 ```<br/>
> ``` the limit is 9 ```
> ``` [0, 1, 1, 2, 3, 5, 8, 13, 21] ```

## TEST 4
Web sederhana yang menyimpan dan menampilkan berapa kali halaman tersebut sudah di lihat.<br/>
Jalankan program dengan:
- jalankan server lokal:
> ``` python3 Test-4/manage.py runserver ```
- Halaman dapat diakses di http://127.0.0.1:8000/

## TEST 5
program konversi video .mp4 menjadi format .hls

### Persiapan:
- install FFmpeg
  - tutorial untuk windows: <br/> 
  https://www.youtube.com/watch?v=UDIMVp4jWXo&list=PLErU2HjQZ_ZOPDZ71Khzt5PX4X4j6flkg&index=2
  - tutorial untuk mac: <br/> 
  https://www.youtube.com/watch?v=UDIMVp4jWXo&list=PLErU2HjQZ_ZOPDZ71Khzt5PX4X4j6flkg&index=2

### Step:
> ``` python3 Test-5/video_converter.py ``` <br/>
- File explorer/ finder/ python akan muncul, pilih file .mp4 yang ingin di konversikan
> ``` Open file /Users/pandai/Downloads/Sample Videos 2.mp4 ``` <br/>
- File .hls akan muncul di satu folder dengan .mp4 dalam 3 format(360p, 480p, 720p)
