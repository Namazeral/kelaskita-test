def reverse_string(input_string):
    new_string = None
    split_string = input_string.split(' ')
    for s in split_string:
        new_word = s[::-1]
        new_string = str(new_word) if not new_string else str(new_string) + ' ' + str(new_word)

    return new_string

if __name__ == '__main__':
    input_string = input('Enter the string:')
    new_string = reverse_string(input_string)
    print(new_string)
