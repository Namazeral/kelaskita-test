import tkinter as tk
from tkinter.filedialog import askopenfilename
import os
import ffmpeg_streaming
from ffmpeg_streaming import Formats, Bitrate, Representation, Size
import sys

if __name__ == '__main__':
    app = tk.Tk()
    app.withdraw()  # use to hide tkinter window

    currdir = os.getcwd()
    tempdir = askopenfilename(filetypes=[("Video Files", ".mp4")])
    if len(tempdir) > 0:
        print("Open file %s" % tempdir)
    else:
        print("File not found")
        print("exit system")
        sys.exit()

    # Generate representations manually:
    _360p = Representation(Size(640, 360), Bitrate(276 * 1024, 128 * 1024))
    _480p = Representation(Size(854, 480), Bitrate(750 * 1024, 192 * 1024))
    _720p = Representation(Size(1280, 720), Bitrate(2048 * 1024, 320 * 1024))

    video = ffmpeg_streaming.input(tempdir)
    hls = video.hls(Formats.h264())
    hls.representations(_360p, _480p, _720p)
    hls.fragmented_mp4()
    hls.output()

