import sys

if __name__ == '__main__':
    limit = input('Input the limit(default is 9):')
    if not limit:
        limit = 9
    # check if the limit is valid
    try:
        limit = int(limit)
        print('the limit is ' + str(limit))
    except:
        print('The input has to be a number!')
        sys.exit()
    if limit <= 0:
        print("The limit can't be below 1!")
        sys.exit()
    list_sequence = []
    # start sequence
    if limit == 1:
        list_sequence.append(0)
        sys.exit()
    n1, n2 = 0, 1
    count = 0
    for x in range(limit):
        list_sequence.append(n1)
        n3 = n1 + n2
        # update values
        n1 = n2
        n2 = n3
    print(list_sequence)